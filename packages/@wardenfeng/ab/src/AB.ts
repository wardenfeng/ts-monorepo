import { A } from '@wardenfeng/a';
import { B } from '@wardenfeng/b';

export class AB
{
    a = new A();
    b = new B();
}
console.log('init AB');
